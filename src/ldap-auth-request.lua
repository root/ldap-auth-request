local require, type, io, tostring, string, assert, pcall = require, type, io, tostring, string, assert, pcall

local pretty = require("pl.pretty")
local mime = require("mime")
local lualdap = require("lualdap")

local _M = {}

-- print message to stderr
local log = function(msg)
    if type(msg) == "table" then
        io.stderr:write("LOG: " .. pretty.write(msg) .. "\n")
    else
        io.stderr:write("LOG: " .. tostring(msg) .. "\n")
    end
end

-- decode HTTP_AUTHORIZATION header and split to login/password
local decode_authorization = function(header)
    local auth_string_b64 = string.match(header, "^Basic%s([^%s]+)$")
    local auth_string, _ = mime.unb64(auth_string_b64)
    local login, password = string.match(auth_string, "^([^:]+)[:](.+)$")
    assert(login ~= nil and login ~= "", [[Login empty!]])
    assert(password ~= nil and password ~= "", [[Password empty!]])
    return login, password
end

-- try connect and bind to ADSI/LDAP
local bind = function(dc, username, password)
    local conn = assert(lualdap.open(dc))
    local _, err = conn:bind_simple(username, password)
    conn:close()
    assert(err == nil, err)
end

-- main
function _M.run(wsapi_env)
    local path = wsapi_env.PATH_INFO
    local authorization = wsapi_env.HTTP_AUTHORIZATION or ""
    local dc = wsapi_env.HTTP_X_LDAP_AUTH_SERVER or "localhost"
    local login_prefix = wsapi_env.HTTP_X_LDAP_AUTH_PREFIX or ""
    local login_suffix = wsapi_env.HTTP_X_LDAP_AUTH_SUFFIX or ""

    -- debug only
    -- log(wsapi_env)

    -- test auth url
    if path ~= "/auth" then
        return 404, {}, ""
    end

    -- test auth header
    if authorization == "" then
        return 401, {["WWW-Authenticate"] = [[Basic realm="realm", charset="UTF-8"]]}, ""
    end

    local status, login, password = pcall(decode_authorization, authorization)
    if (not status) then
        return 401, {["WWW-Authenticate"] = [[Basic realm="realm", charset="UTF-8"]]}, ""
    end

    -- make normal username
    local username = string.format("%s%s%s", login_prefix, login, login_suffix)

    local status, err = pcall(bind, dc, username, password)
    if (not status) then
        log(err)
        return 401, {["WWW-Authenticate"] = [[Basic realm="realm", charset="UTF-8"]]}, ""
    end

    return 200, {["X-LDAP-Auth-Login"] = string.lower(login)}, ""
end

return _M
