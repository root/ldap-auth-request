package = "ldap-auth-request"
version = "0.0.1-1"
source = {
    url = "https://git.wan-tech.ru/root/ldap-auth-request.git"
}
description = {
    summary = "summary",
    detailed = "detailed",
    homepage = "https://git.wan-tech.ru/root/ldap-auth-request.git",
    license = "MIT/X11"
}
-- luarocks install --only-deps <rockspec_file>
dependencies = {
    "lua >= 5.1, < 5.2",
    "lualdap >= 1.3.0",
    "luasocket >= 2.0.2",
    "penlight >= 1.10.0"
}
build = {
    type = "builtin",
    modules = {}
}
